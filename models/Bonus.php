<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 *
 * @property Bonusreason $reason
 * @property User $user
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'reasonId', 'amount'], 'required'],
            [['id', 'userId', 'reasonId', 'amount'], 'integer'],
            [['reasonId'], 'exist', 'skipOnError' => true, 'targetClass' => Bonusreason::className(), 'targetAttribute' => ['reasonId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],

           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',                       
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(Bonusreason::className(), ['id' => 'reasonId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function getBonusItem()
       {
           return $this->hasOne(Bonusreason::classname(), ['id' => 'reasonId']);
       }

}
